package com.tr.example.notificationservice.domain.service.builder.impl;

import com.tr.example.notificationservice.config.model.dto.EventDto;
import com.tr.example.notificationservice.domain.model.dto.NotificationContextDto;
import com.tr.example.notificationservice.domain.service.builder.NotificationContextBuilder;
import org.springframework.stereotype.Service;

@Service
public class ProductCreatedNotificationBuilder implements NotificationContextBuilder {
    @Override
    public boolean check(String eventType) {
        return false;
    }

    @Override
    public NotificationContextDto build(String eventType, EventDto event) {
        return null;
    }
}
