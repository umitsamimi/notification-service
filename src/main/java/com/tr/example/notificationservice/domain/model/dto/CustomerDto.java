package com.tr.example.notificationservice.domain.model.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {
    private Long id;
    private Long customerNo;
    private String firstName;
    private String lastName;
    private String email;
}