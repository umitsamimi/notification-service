package com.tr.example.notificationservice.domain.model.dto;

import com.tr.example.notificationservice.domain.model.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerNotificationEventDto {
    private Long customerId;
    private Currency currency;
    private Integer status;
    private String phoneNumber;
}