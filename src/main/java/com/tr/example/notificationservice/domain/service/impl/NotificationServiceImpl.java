package com.tr.example.notificationservice.domain.service.impl;

import com.tr.example.notificationservice.config.model.dto.EventDto;
import com.tr.example.notificationservice.domain.dao.NotificationTemplateDao;
import com.tr.example.notificationservice.domain.model.NotificationTemplateDto;
import com.tr.example.notificationservice.domain.model.dto.NotificationContextDto;
import com.tr.example.notificationservice.domain.model.dto.NotificationEventNotifyMethodDto;
import com.tr.example.notificationservice.domain.model.enums.Language;
import com.tr.example.notificationservice.domain.model.enums.NotificationEvent;
import com.tr.example.notificationservice.domain.service.builder.NotificationContextBuilder;
import com.tr.example.notificationservice.domain.service.NotificationService;
import com.tr.example.notificationservice.domain.dao.NotifiationEventNotifyMethodDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
class NotificationServiceImpl implements NotificationService {

    private final NotifiationEventNotifyMethodDao notifiationEventNotifyMethodDao;

    private final NotificationTemplateDao notificationTemplateDao;

    private final List<NotificationContextBuilder> builders;
    @Override
    public void notify(EventDto event) {
        Optional<NotificationEventNotifyMethodDto> optNotificationEventNotifyMethod = notifiationEventNotifyMethodDao
                .getNotificationEventNotifyMethod(getNotifivationEventId(event.getType()));
        if(optNotificationEventNotifyMethod.isPresent()){
            NotificationEventNotifyMethodDto notificationEventNotifyMethod = optNotificationEventNotifyMethod.get();
            Optional<NotificationContextDto> optionalNotificationContext = builders.stream()
                    .filter(builder -> builder.check(event.getType()))
                    .map(builder -> builder.build(event.getType(), event))
                    .findFirst();

            if(optionalNotificationContext.isPresent()){
                NotificationContextDto notificationContext = optionalNotificationContext.get();
                Long customerNo = Long.valueOf(notificationContext.getCustomer().getCustomerNo());
                Language customerCurrentLanguage = getCustomerCurrentLanguage(customerNo);

                Optional<NotificationTemplateDto> optionalNotificationTemplate = notificationTemplateDao
                        .getNotificationTemplate(notificationEventNotifyMethod.getNotificationTemplateCode(), customerCurrentLanguage);
                if(optionalNotificationTemplate.isPresent()){
                    NotificationTemplateDto notificationTemplate = optionalNotificationTemplate.get();
                    replaceVariables(notificationTemplate, notificationContext.getVariables());
                }
                /*
                    SEND Notification
                 */

            }
        }
    }

    private void replaceVariables(NotificationTemplateDto notificationTemplate, Map<String, String> variables) {

    }

    private Language getCustomerCurrentLanguage(Long customerNo) {
        return Language.TR;
    }

    private Integer getNotifivationEventId(String eventType){
        Optional<NotificationEvent> optionalNotificationEvent = Arrays.stream(NotificationEvent.values())
                .filter(item -> item.name().equals(eventType))
                .findFirst();

        if(optionalNotificationEvent.isPresent()){
            return optionalNotificationEvent.get().getEventId();
        }
        return 0; //TODO DEFAULT
    }
}
