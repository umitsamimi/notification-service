package com.tr.example.notificationservice.domain.repository;

import com.tr.example.notificationservice.domain.model.entity.NotificationTemplateEntity;
import com.tr.example.notificationservice.domain.model.enums.Language;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotificationTemplateRepository extends CrudRepository<NotificationTemplateEntity, Long> {

    Optional<NotificationTemplateEntity> findByCodeAndAndLanguage(Long code, Language language);
}