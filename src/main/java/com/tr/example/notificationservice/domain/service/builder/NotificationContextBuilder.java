package com.tr.example.notificationservice.domain.service.builder;

import com.tr.example.notificationservice.config.model.dto.EventDto;
import com.tr.example.notificationservice.domain.model.dto.NotificationContextDto;

public interface NotificationContextBuilder {

    boolean check(String eventType);

    NotificationContextDto build(final String eventType, final EventDto event);
}
