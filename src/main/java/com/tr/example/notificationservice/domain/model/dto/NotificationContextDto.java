package com.tr.example.notificationservice.domain.model.dto;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationContextDto {
    private Map<String, String> variables;
    private CustomerDto customer;
    private ProductDto product;
}
