package com.tr.example.notificationservice.domain.model.enums;

public enum NotifyMethod {
    SMS,
    EMAIL
}