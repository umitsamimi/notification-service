package com.tr.example.notificationservice.domain.model.dto;

import com.tr.example.notificationservice.domain.model.enums.NotifyMethod;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationEventNotifyMethodDto {

    private Long id;
    private Integer notificationEventId;
    private NotifyMethod method;
    private Long notificationTemplateCode;
}
