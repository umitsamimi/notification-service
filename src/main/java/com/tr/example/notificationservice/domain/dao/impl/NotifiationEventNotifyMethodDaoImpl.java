package com.tr.example.notificationservice.domain.dao.impl;

import com.tr.example.notificationservice.domain.dao.NotifiationEventNotifyMethodDao;
import com.tr.example.notificationservice.domain.model.dto.NotificationEventNotifyMethodDto;
import com.tr.example.notificationservice.domain.model.entity.NotificationEventNotifyMethodEntity;
import com.tr.example.notificationservice.domain.repository.NotificationEventNotifyMethodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
class NotifiationEventNotifyMethodDaoImpl implements NotifiationEventNotifyMethodDao {

    private final NotificationEventNotifyMethodRepository notificationEventNotifyMethodRepository;
    @Override
    public Optional<NotificationEventNotifyMethodDto> getNotificationEventNotifyMethod(Integer notificationEventId) {
        Optional<NotificationEventNotifyMethodEntity> optionalNotificationEventNotifyMethod = notificationEventNotifyMethodRepository
                .findByNotificationEventId(notificationEventId);
        if(optionalNotificationEventNotifyMethod.isEmpty()){

        }
        return Optional.of(NotificationEventNotifyMethodDto.builder()
                .method(optionalNotificationEventNotifyMethod.get().getMethod())
                .id(optionalNotificationEventNotifyMethod.get().getId())
                .notificationEventId(optionalNotificationEventNotifyMethod.get().getNotificationEventId())
                .notificationTemplateCode(optionalNotificationEventNotifyMethod.orElseThrow().getNotificationTemplateCode())
                .build());
    }
}
