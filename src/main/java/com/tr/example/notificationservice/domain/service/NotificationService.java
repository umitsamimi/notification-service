package com.tr.example.notificationservice.domain.service;

import com.tr.example.notificationservice.config.model.dto.EventDto;

public interface NotificationService {
    void notify(EventDto event);
}
