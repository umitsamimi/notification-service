package com.tr.example.notificationservice.domain.model.enums;

import lombok.Getter;

@Getter
public enum NotificationEvent {
    DEFAULT(0),
    CUSTOMER_REGISTER_SUCCESS(1),
    PRODUCT_CREATED_SUCCESS(2),
    PRODUCT_APPROVED(3);
    private Integer eventId;
    NotificationEvent(Integer eventId) {
        this.eventId = eventId;
    }

    public static NotificationEvent getById(Integer eventId) {
        for (NotificationEvent event : NotificationEvent.values()) {
            if (event.getEventId().equals(eventId)) {
                return event;
            }
        }
        return DEFAULT;
    }
}