package com.tr.example.notificationservice.domain.service.builder.impl;

import com.tr.example.notificationservice.config.model.dto.EventDto;
import com.tr.example.notificationservice.delegate.service.CustomerDelegateService;
import com.tr.example.notificationservice.domain.model.dto.CustomerDto;
import com.tr.example.notificationservice.domain.model.dto.CustomerNotificationEventDto;
import com.tr.example.notificationservice.domain.model.dto.NotificationContextDto;
import com.tr.example.notificationservice.domain.model.enums.NotificationEvent;
import com.tr.example.notificationservice.domain.service.builder.NotificationContextBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
class CustomerRegisterNotificationBuilder implements NotificationContextBuilder {

    private final CustomerDelegateService customerDelegateService;

    private final ObjectMapper mapper;

    @Override
    public boolean check(String eventType) {
        return NotificationEvent.CUSTOMER_REGISTER_SUCCESS.name().equals(eventType) ? true : false;
    }

    @Override
    public NotificationContextDto build(String eventType, EventDto event) {
        if(check(eventType)){
            final CustomerNotificationEventDto customerNotificationEvent = mapper
                    .convertValue(event.getEventData(), CustomerNotificationEventDto.class);

            CustomerDto customer = customerDelegateService
                    .getCustomer(Long.valueOf(customerNotificationEvent.getCustomerId()));
            if(Objects.nonNull(customer)){
                NotificationContextDto context = NotificationContextDto.builder()
                        .customer(CustomerDto.builder()
                                .id(customer.getId())
                                .customerNo(customer.getCustomerNo())
                                .email(customer.getEmail())
                                .firstName(customer.getFirstName())
                                .build())
                        .build();
                setVariables(context);
            }
        }

        return null;
    }

    protected void setVariables(NotificationContextDto context) {
        context.getVariables().put("$customerNo", context.getCustomer().getCustomerNo().toString());
        context.getVariables().put("$customerFirstName", context.getCustomer().getFirstName());
        context.getVariables().put("$customerLastName", context.getCustomer().getLastName());
        context.getVariables().put("$customerEmail", context.getCustomer().getEmail());
    }
}
