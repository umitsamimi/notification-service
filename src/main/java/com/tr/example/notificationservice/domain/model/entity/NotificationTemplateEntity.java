package com.tr.example.notificationservice.domain.model.entity;

import com.tr.example.notificationservice.domain.model.enums.Language;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "notification_template")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationTemplateEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long code;
    @Enumerated(EnumType.STRING)
    private Language language;
    private String title;
    private String content;
    private String summary;
}