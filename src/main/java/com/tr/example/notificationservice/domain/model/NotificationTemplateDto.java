package com.tr.example.notificationservice.domain.model;

import com.tr.example.notificationservice.domain.model.enums.Language;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationTemplateDto implements Serializable {
    private Long id;
    private Long code;
    private Language language;
    private String title;
    private String content;
    private String summary;
}
