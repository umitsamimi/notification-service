package com.tr.example.notificationservice.domain.model.enums;

public enum Currency {
    TL,
    EUR;
}
