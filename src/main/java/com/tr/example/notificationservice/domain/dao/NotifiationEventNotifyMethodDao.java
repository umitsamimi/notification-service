package com.tr.example.notificationservice.domain.dao;

import com.tr.example.notificationservice.domain.model.dto.NotificationEventNotifyMethodDto;

import java.util.Optional;

public interface NotifiationEventNotifyMethodDao {

    Optional<NotificationEventNotifyMethodDto> getNotificationEventNotifyMethod(Integer notificationEventId);
}