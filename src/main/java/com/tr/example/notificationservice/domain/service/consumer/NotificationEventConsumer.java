package com.tr.example.notificationservice.domain.service.consumer;

import com.tr.example.notificationservice.config.model.dto.EventDto;
import com.tr.example.notificationservice.domain.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationEventConsumer {
    private final NotificationService notificationService;

    @KafkaListener(topics = {"notification.user.notify"},
            containerFactory = "notificationEventConsumerListenerFactory")
    public void consume(EventDto event) {
        notificationService.notify(event);
    }
}