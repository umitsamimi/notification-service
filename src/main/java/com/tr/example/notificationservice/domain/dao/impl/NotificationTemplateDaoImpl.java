package com.tr.example.notificationservice.domain.dao.impl;

import com.tr.example.notificationservice.domain.dao.NotificationTemplateDao;
import com.tr.example.notificationservice.domain.model.NotificationTemplateDto;
import com.tr.example.notificationservice.domain.model.entity.NotificationTemplateEntity;
import com.tr.example.notificationservice.domain.model.enums.Language;
import com.tr.example.notificationservice.domain.repository.NotificationTemplateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NotificationTemplateDaoImpl implements NotificationTemplateDao {
    private final NotificationTemplateRepository notificationTemplateRepository;

    @Override
    public Optional<NotificationTemplateDto> getNotificationTemplate(final Long code, final Language language){
        Optional<NotificationTemplateEntity> optionalNotificationTemplate = notificationTemplateRepository.findByCodeAndAndLanguage(code, language);
        if(optionalNotificationTemplate.isPresent()){
            NotificationTemplateEntity notificationTemplateEntity = optionalNotificationTemplate.get();
            return Optional.of(NotificationTemplateDto.builder()
                    .code(notificationTemplateEntity.getCode())
                    .id(notificationTemplateEntity.getId())
                    .content(notificationTemplateEntity.getContent())
                    .summary(notificationTemplateEntity.getSummary())
                    .title(notificationTemplateEntity.getTitle())
                    .language(notificationTemplateEntity.getLanguage())

                    .build());
        }
        return Optional.empty();
    }
}
