package com.tr.example.notificationservice.domain.dao;

import com.tr.example.notificationservice.domain.model.NotificationTemplateDto;
import com.tr.example.notificationservice.domain.model.enums.Language;

import java.util.Optional;

public interface NotificationTemplateDao {
    Optional<NotificationTemplateDto> getNotificationTemplate(Long code, Language language);
}