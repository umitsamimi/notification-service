package com.tr.example.notificationservice.domain.model.entity;

import com.tr.example.notificationservice.domain.model.enums.NotifyMethod;
import lombok.*;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "notification_event_notify_method")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationEventNotifyMethodEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer notificationEventId;

    @Enumerated(EnumType.STRING)
    private NotifyMethod method;

    private Long notificationTemplateCode;
}