package com.tr.example.notificationservice.domain.repository;

import com.tr.example.notificationservice.domain.model.entity.NotificationEventNotifyMethodEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotificationEventNotifyMethodRepository extends CrudRepository<NotificationEventNotifyMethodEntity, Long> {
    Optional<NotificationEventNotifyMethodEntity> findByNotificationEventId(Integer notificationEventId);
}
