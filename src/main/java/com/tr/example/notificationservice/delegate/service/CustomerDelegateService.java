package com.tr.example.notificationservice.delegate.service;

import com.tr.example.notificationservice.domain.model.dto.CustomerDto;

public interface CustomerDelegateService {
    CustomerDto getCustomer(Long Id);
}
