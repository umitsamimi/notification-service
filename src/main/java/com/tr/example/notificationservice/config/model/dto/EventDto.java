package com.tr.example.notificationservice.config.model.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventDto {
    private String eventName;
    private String topicName;
    private String type;
    private String layer;
    private Long createTime;
    private Object eventData;
}
